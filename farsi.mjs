/**
 * Convert english numbers to farsi numbers
 *
 * @param {Number} number - An english number
 * @return {String}
 */
export function NumberToFarsi(number) {
  if (number == null)
    throw new Error("Number is empty")

  let strNumber
  if (typeof(number) === "number")
    strNumber = String(number)
  else if (typeof(number) === "string") {
    let numberRX = /^\d+$/
    if (!numberRX.test(number))
      throw new Error("Number is invalid")
    strNumber = number
  } else
    throw new Error("Number is invalid")

  const farsiNumberCodePoint = 1728

  return strNumber
    .split("")
    .reduce(
      (accumulator, currentChar) =>
        accumulator + String.fromCharCode(currentChar.charCodeAt() + farsiNumberCodePoint),
      ""
    )
}
