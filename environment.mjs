/**
 * A dictionary of environments
 */
const environments = {
  DEVELOPMENT: "DEVELOPMENT",
  TEST:        "TEST",
  STAGING:     "STAGING",
  PRODUCTION:  "PRODUCTION"
}


/**
 * Get current environment
 */
const getEnvironment = () => {
  if (!Object.prototype.hasOwnProperty.call(import.meta, "env") || !Object.prototype.hasOwnProperty.call(import.meta.env, "MODE"))
    return environments.DEVELOPMENT

  switch (import.meta.env.MODE.toUpperCase()) {
  case "TEST":
    return environments.TEST
  case "PROD":
  case "PRODUCTION":
    return environments.PRODUCTION
  case "STAGING":
    return environments.STAGING
  case "DEV":
  case "DEVELOP":
  case "DEVELOPMENT":
  default:
    return environments.DEVELOPMENT
  }
}



Object.assign(globalThis, {

  /**
   * Environments
   */
  environments,

  /**
   * Current Environment
   */
  environment: getEnvironment()

})
