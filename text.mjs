import { Formatter }  from "./common.mjs"

/*
 * Importing "texts" resource file
 */
import textsResource  from "@/assets/texts?raw"


/*
 * Regular Expressions
 */
const regExperssions = {
  separator : /^[:=|;,.?@#%&*]+$/,
  comment   : /^[:=|;,.?@#%&*]+$/,
  newline   : /\r\n|\n|\r/gm
}

/*
 * Error Messages
 */
const errorMessages = {
  separatorString:            "The separator parameter must be string type",
  separatorInvalidCharacter:  "The separator parameter must be one or more of characters (:=|;.,?@#%&*) delimiter",
  commentString:              "The comment parameter must be string type",
  commentInvalidCharacter:    "The comment parameter must be one or more of characters (:=|;.,?@#%&*) delimiter",
  stringKeyNotFound:          "Error in string. KEY not found",
  stringSeparatorNotFound:    "Error in string. SEPARATOR not found"
}


/**
 * Texts resource management
 *
 * Create a "texts" file in "@/assets" folder
 * Add KEY=VALUE strings to text file
 * Use `#` for comment
 *
 * Call:
 * ```js
 * this.$Text(Key)
 * this.$Text(Key, param1, param2, ...)
 * ```
 *
 * @param {String} separator - One or more character separator (:=|;.,?@#%&*)
 * @param {String} comment - One or more character separator (:=|;.,?@#%&*)
 */
export default {
  install(app, { separator = "=", comment = "#" } = {}) {

    if (typeof(separator) !== "string")
      throw new Error(errorMessages.separatorString)
    if (regExperssions.separator.test(separator) === false)
      throw new Error(errorMessages.separatorInvalidCharacter)
    if (typeof(comment) !== "string")
      throw new Error(errorMessages.commentString)
    if (regExperssions.comment.test(comment) === false)
      throw new Error(errorMessages.commentInvalidCharacter)

    const strings = new Map()

    ;(() => {
      textsResource
        .split(regExperssions.newline)
        .map(line => line.trim())
        .filter(line => line && line.length > 0 && !line.startsWith(comment))
        .forEach(line => {
          const indexOfKey = line.indexOf(separator)
          if (indexOfKey === 0)
            throw new Error(errorMessages.stringKeyNotFound)
          if (indexOfKey === -1)
            throw new Error(errorMessages.stringSeparatorNotFound)

          strings.set(
            line.slice(0, indexOfKey).trim(),
            line.slice(indexOfKey + separator.length).trimStart()
          )
        })
    })()

    function Texter(name, ...places) {
      if (!strings.has(name))
        return name
      return Formatter(strings.get(name), ...places)
    }

    app.config.globalProperties.$Text = Texter
    app.$Text = Texter
  }
}
